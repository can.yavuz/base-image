FROM debian:stretch-slim

ARG BUNDLE_DIR

ENV YQ_VERSION=v4.12.0
ENV YQ_BINARY=yq_linux_amd64

RUN apt-get update && apt-get install -y ca-certificates wget patch curl \
    && (wget -O - "https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/${YQ_BINARY}.tar.gz" | tar xz) \
    && mv ${YQ_BINARY} /usr/bin/yq \
    && curl https://get.helm.sh/helm-v3.3.4-linux-amd64.tar.gz --output helm3.tar.gz \
    && tar -xvf helm3.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm3 \
    && rm -R linux-amd64
